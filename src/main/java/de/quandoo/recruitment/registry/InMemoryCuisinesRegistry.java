package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {
    private final Map<Cuisine, List<Customer>> customersByCuisineIndex = new HashMap<>();
    private final Map<Customer, List<Cuisine>> cuisineByCustomerIndex = new HashMap<>();

    @Override
    public void register(final Customer userId, final Cuisine cuisine) {
        cuisineByCustomerIndex.computeIfAbsent(userId, (x) -> new LinkedList<>()).add(cuisine);
        customersByCuisineIndex.computeIfAbsent(cuisine, (x) -> new LinkedList<>()).add(userId);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return customersByCuisineIndex.getOrDefault(cuisine, Collections.EMPTY_LIST);
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        return cuisineByCustomerIndex.getOrDefault(customer, Collections.EMPTY_LIST);
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        return customersByCuisineIndex.entrySet().stream().sorted((x, y) -> y.getValue().size() - x.getValue().size())
                .limit(n).map(Map.Entry::getKey).collect(Collectors.toList());
    }
}
