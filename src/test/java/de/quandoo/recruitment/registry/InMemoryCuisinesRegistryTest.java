package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void makeSenseTest() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("french"));

        List<Customer> french = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));

        Assert.assertEquals(2, french.size());
        Assert.assertTrue("Customers list does not contain first customer", french.contains(new Customer("1")));
        Assert.assertTrue("Customers list does not contain fourth customer", french.contains(new Customer("4")));
    }

    @Test
    public void edgeTest1() {
        Assert.assertEquals(Collections.EMPTY_LIST, cuisinesRegistry.cuisineCustomers(null));
    }

    @Test
    public void edgeTest2() {
        Assert.assertEquals(Collections.EMPTY_LIST, cuisinesRegistry.customerCuisines(null));
    }

    @Test
    public void chartTest() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("french"));

        List<Cuisine> top1 = cuisinesRegistry.topCuisines(1);
        Assert.assertEquals(1, top1.size());
        Assert.assertEquals("french", top1.get(0).getName());
    }
}